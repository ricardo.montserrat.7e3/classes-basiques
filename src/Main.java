import java.io.*;
import java.util.*;

public class Main
{
    private void option1()
    {
        File dirPath = new File(inputAny("Insert an existent directory path: "));

        if (dirPath.exists() && dirPath.isDirectory())
        {
            try
            {
                File[] contents = dirPath.listFiles();
                System.out.println(dirPath.getName() +
                        "---------------------------------");
                assert contents != null;
                for (File content : contents)
                {
                    if (content.isFile())
                        System.out.println("[FILE]" + content.getName());
                    else
                        System.out.println("[DIR]" + content.getName());
                }
            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
        else
            System.out.println("DIRECTORY PATH DOESN'T EXIST!");
    }

    private void option2()
    {
        try
        {
            File userFile = new File(inputAny("Write the file name: ") + ".txt");
            FileWriter writer = new FileWriter(userFile);

            int limit = inputInt("How many lines will you write: ");
            for (int i = 1; i <= limit; i++)
            {
                writer.write(inputAny("Text for Line " + i + ":\n") + "\n");
            }
            writer.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private void option3()
    {
        File path = new File(inputAny("Insert an existent file path: "));

        if (path.exists() && path.isFile())
        {
            try
            {
                FileWriter writer = new FileWriter(path, true);

                int limit = inputInt("How many lines will you add: ");
                for (int i = 1; i <= limit; i++)
                {
                    writer.write(inputAny("Text for Line " + i + ":\n") + "\n");
                }
                writer.close();
            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
        else
            System.out.println("FILE PATH DOESN'T EXIST!");
    }

    private void option4()
    {
        File path = new File(inputAny("Insert an existent file path: "));

        if (path.exists() && path.isFile())
        {
            try
            {
                FileReader fr = new FileReader(path);
                int fChar = fr.read();

                while (fChar != -1)
                {
                    System.out.print((char) fChar);
                    fChar = fr.read();
                }

                fr.close();

            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
        else
            System.out.println("FILE PATH DOESN'T EXIST!");
    }

    private void option6()
    {
        File path = new File(inputAny("Insert an existent file path: "));

        if (path.exists() && path.isFile())
        {
            try
            {
                Scanner reader = new Scanner(path);

                String line = reader.nextLine();

                while (line != null)
                {
                    System.out.println(line);
                    line = reader.nextLine();
                }

            }
            catch (Exception e)
            {
                System.out.println("\nNo more lines found.");
            }
        }
        else
            System.out.println("FILE PATH DOESN'T EXIST!");
    }

    private void option7()
    {
        File path = new File("enters.txt");

        if (path.exists() && path.isFile())
        {
            try
            {
                FileReader reader = new FileReader(path);
                System.out.println("\n-----------------------------------");
                StringBuilder nums = new StringBuilder();
                int sum = 0, i = 0;
                int fInt = reader.read();
                while (fInt != -1)
                {
                    System.out.print((char) fInt);
                    if ((char) fInt == ' ' || (char) fInt == '\n')
                    {
                        nums.append(sum);
                        nums.append((char) fInt);
                        sum = 0;
                        i++;
                    }
                    else
                        sum += Character.getNumericValue((char) fInt);

                    fInt = reader.read();
                }
                System.out.println("\n-----------------------------------\n" +
                        "There are: " + (i - 1) + " whole numbers.\n\n" +
                        "The sum of each number is:\n" + nums);
            }
            catch (Exception e)
            {
                System.out.println(e.toString());
            }
        }
        else
            System.out.println("THE FILE [enters.txt] IS NOT IN THE PROJECT'S DIRECTORY!");
    }

    private void option8()
    {
        File path = new File(inputAny("Insert an existent file path: "));

        if (path.exists() && path.isFile())
        {
            String longest = "";
            try
            {
                Scanner reader = new Scanner(path);

                String line = reader.nextLine();

                while (line != null)
                {
                    if (line.length() >= longest.length())
                        longest = line;
                    line = reader.nextLine();
                }

            }
            catch (Exception e)
            {
                System.out.println("The longest line is: " + longest);
            }
        }
        else
            System.out.println("FILE PATH DOESN'T EXIST!");
    }

    private void option9()
    {
        File path = new File(inputAny("Insert an existent file path: "));

        if (path.exists() && path.isFile())
        {
            if (path.delete())
                System.out.println("The [FILE]" + path.getName() + " was succesfully deleted!");
            else
                System.out.println("Couldn't delete the file.");
        }
        else
            System.out.println("FILE PATH DOESN'T EXIST!");
    }

    private void option11()
    {
        File path = new File(inputAny("Insert an existent path: "));

        if (path.exists())
        {
            if (path.isFile())
                System.out.println("[FILE]" + path.getName());
            else
                System.out.println("[DIR]" + path.getName());
            System.out.println("[SIZE]: " + path.length() / 1024 + "kb" +
                    "\n[PERMISSIONS]: ");
            if (path.canExecute())
                System.out.println("- Can Execute");
            if (path.canRead())
                System.out.println("- Can Read");
            if (path.canWrite())
                System.out.println("- Can Write");
        }
        else
            System.out.println("The path doesnt't exist!");
    }

    private void option12()
    {
        try
        {
            BufferedReader csvReader = new BufferedReader(new FileReader("restaurants.csv"));
            String row;
            while ((row = csvReader.readLine()) != null)
            {
                String[] data = row.split(",");
                for (String value : data)
                    System.out.println(value);
            }
            csvReader.close();
        }
        catch (Exception e)
        {
            System.out.println("There is no [FILE]restaurants.csv in the project");
        }
    }

    private void option13()
    {
        //Nom,Telefon,Direccio,Districte,Barri,Ciutat,Codi Postal,Regio,Pais,Lat,Lon,Web,Mail
        Hashtable<String, String> dictionary = new Hashtable<String, String>();
        int rowsLimit = inputInt("How many rows will you write: ");

        try
        {
            FileWriter fr = new FileWriter("restaurants.csv", true);

            for (int i = 0; i < rowsLimit; i++)
            {
                dictionary.put("Name", inputAny("Name: "));
                dictionary.put("Phone", inputAny("Phone: "));
                dictionary.put("Address", inputAny("Address: "));
                dictionary.put("District", inputAny("District: "));
                dictionary.put("Hood", inputAny("Hood: "));
                dictionary.put("City", inputAny("City: "));
                dictionary.put("Postal Code", inputAny("Postal Code: "));
                dictionary.put("Region", inputAny("Region: "));
                dictionary.put("Country", inputAny("Country: "));
                dictionary.put("Lat", inputAny("Lat: "));
                dictionary.put("Lon", inputAny("Lot: "));
                dictionary.put("Web", inputAny("Web: "));
                dictionary.put("Mail", inputAny("Mail: "));

                dictionary.forEach((key, value) ->
                {
                    try
                    {
                        fr.append(value);
                        if (!key.equals("Mail"))
                            fr.append(',');
                        else
                            fr.append("\n");
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                });
            }
            fr.flush();
            fr.close();
        }
        catch (Exception e)
        {
            System.out.println("There is no [FILE]restaurants.csv in the project");
        }
    }

    private void option14()
    {
        try
        {
            BufferedReader csvReader = new BufferedReader(new FileReader("restaurants.csv"));
            FileWriter fr = new FileWriter("restaurants2.csv");
            File newRestaurantCsv = new File("restaurants2.csv");
            String row;
            while ((row = csvReader.readLine()) != null)
            {
                if(!row.contains("Eixample"))
                    fr.write(row);
            }
            fr.flush();
            fr.close();
            csvReader.close();
        }
        catch (Exception e)
        {
            System.out.println("There is no [FILE]restaurants.csv in the project");
        }
    }

    private void pause()
    {
        Scanner reader = new Scanner(System.in);
        System.out.print("\n\nPress enter to continue...");
        reader.nextLine();
    }

    private String inputAny(String message)
    {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);
        return reader.nextLine();
    }

    private int inputInt(String message)
    {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);

        int num = 0;
        boolean isNum;
        do
        {
            try
            {
                num = reader.nextInt();
                isNum = true;
            }
            catch (Exception e)
            {
                System.out.print(message);
                isNum = false;
            }
            reader.nextLine();
        }
        while (!isNum);

        return num;
    }

    private int getAction(int option)
    {
        switch (option)
        {
            case 0: return 0;
            case 1: option1();
                break;
            case 2: option2();
                break;
            case 3: option3();
                break;
            case 4: option4();
                break;
            case 5: return 1;
            case 6: option6();
                break;
            case 7: option7();
                break;
            case 8: option8();
                break;
            case 9: option9();
                break;
            case 10: return 2;
            case 11: option11();
                break;
            case 12: option12();
                break;
            case 13: option13();
                break;
            case 14: option14();
                break;
            case 15: return -1;
            default: break;
        }
        pause();
        return 0;
    }

    private void drawMenu(int option)
    {
        switch (option)
        {
            case 0:
                System.out.print("Welcome To My Application\n" +
                        "--------------------------------------\n\n" +
                        "1.- See files in a directory.\n" +
                        "2.- Create a file and write on it.\n" +
                        "3.- Add lines to an existing file.\n" +
                        "4.- Read a file.\n" +
                        "5.- See more...\n" +
                        "15.- Exit.\n\n");
                break;
            case 1:
                System.out.print("Second Options\n" +
                        "--------------------------------------\n\n" +
                        "0.- Go Back.\n" +
                        "6.- Read a file but with Scanner.\n" +
                        "7.- Read enters.txt file, and get stats.\n" +
                        "8.- Find longest line in a file.\n" +
                        "9.- Delete file.\n" +
                        "10.- See more...\n" +
                        "15.- Exit.\n\n");
                break;
            case 2:
                System.out.print("Third Options\n" +
                        "--------------------------------------\n\n" +
                        "0.- Go Back.\n" +
                        "11.- See stats of file or directory.\n" +
                        "12.- See restaurants.csv.\n" +
                        "13.- Add row to restaurants.csv.\n" +
                        "14.- Create restaurants2.csv without Eixample ones.\n" +
                        "15.- Exit.\n\n");
                break;
        }
    }

    private void startMenu()
    {
        int action = 0;
        do
        {
            drawMenu(action);
            action = getAction(inputInt("Select an option by number: "));
        }
        while (action != -1);

        System.out.println("\n\nGoodbye :(");
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.startMenu();
    }
}
